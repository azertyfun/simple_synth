# simple_synth

This is a very simple monophonic synthesizer implementation in Rust, for the sake of learning new things. It has an oscillator with a low pass filter as well as an ADSR envelope, all of those feeding into an amplificator.

Those modules were implemented as described on [Beau Sievers' blog](http://beausievers.com/synth/synthbasics/).
